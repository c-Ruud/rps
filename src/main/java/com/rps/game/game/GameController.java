
package com.rps.game.game;

import com.rps.game.exceptions.*;
import com.rps.game.token.*;
import com.rps.game.tokenGame.TokenGameEntity;
import com.rps.game.tokenGame.TokenGameRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("/games")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GameController {

    GameService gameService;
    TokenService tokenService;
    TokenRepository tokenRepository;
    GameRepository gameRepository;
    TokenGameRepository tokenGameRepository;

    @GetMapping("/start")
    public GameStatus createGame(@RequestHeader(value = "token", required = true) String tokenId) throws OneGameAtTheTimeAllowedException, JoinerCantHaveTypeOwner {
        GameEntity game = gameService.startGame(tokenId);
        TokenEntity one = tokenRepository.getOne(tokenId);
        System.out.println("game"+game);
        return toGameStatus(game, one);
    }

    @GetMapping("/join/{gameId}")
    public GameStatus joinGame(@PathVariable String gameId, @RequestHeader(value = "token", required = true) String tokenId)
            throws TokenAlreadyJoinedToGameException, GameAlreadyStartedException, JoinerCantHaveTypeOwner {
        return toGameStatus(gameService.joinGame(gameId, tokenId), tokenRepository.getOne(tokenId));
    }

   @GetMapping("/status")
    public GameStatus statusGame(@RequestHeader(value = "token", required = true) String tokenId) {
       return gameService.getAllGamesConnectedToToken(tokenId).stream()
               .map(TokenGameEntity::getGame)
               .map(gameEntity -> toGameStatus(gameEntity,tokenRepository.getOne(tokenId))).findAny().get();
    }

    @GetMapping()
    public List<GameStatus> listOfAllJoinableGames(@RequestHeader(value = "token", required = true) String tokenId) {
        return gameService.allOpenGames(tokenId)
                .map(gameEntity -> toGameStatus(gameEntity, getOwner(gameEntity)))
                .collect(Collectors.toList());

    }
    @GetMapping("/history")
    public List<GameStatus> playedGamesHistory(@RequestHeader(value = "token", required = true) String tokenId) {
        return gameService.getAllEndedGamesForToken(tokenId)
                .map(TokenGameEntity::getGame)
                .map(gameEntity -> toGameStatus(gameEntity, tokenRepository.getOne(tokenId)))
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    public GameStatus getGameStatus(@RequestHeader(value = "token", required = true) String tokenId, @PathVariable String id) {
        GameEntity game = gameRepository.getOne(id);
        return toGameStatus(game, tokenRepository.getOne(tokenId));
    }

    @GetMapping("/move/{sign}")
    public GameStatus makeMove(@PathVariable Sign sign, @RequestHeader(value = "token", required = true) String tokenId) throws GameNotFoundException {
        return toGameStatus(gameService.makeMove(sign, tokenId), tokenRepository.getOne(tokenId));
    }

    private GameStatus toGameStatus(GameEntity game, TokenEntity token) {
        if (game.isOwner(token)) {
            return new GameStatus(
                    game.getId(),
                    token.getName(),
                    game.move().map(Enum::name).orElse(""),
                    game.getGame().name(),
                    game.getOpponentName(),
                    game.opponentMove().map(Enum::name).orElse("")
            );
        }

        return new GameStatus(
                game.getId(),
                token.getName(),
                game.opponentMove().map(Enum::name).orElse(""),
                joinerView(game.getGame()).name(),
                game.getOwnerName(),
                game.move().map(Enum::name).orElse("")
        );
    }

    private Status joinerView(Status status) {
        switch (status) {
            case WIN:
                return Status.LOSE;
            case LOSE:
                return Status.WIN;
            default:
                return status;
        }
    }

    private TokenEntity getOwner(GameEntity gameEntity) {
        return gameEntity.getTokens().stream()
                .filter(tokenGameEntity ->
                        tokenGameEntity.getType()
                                .equals(TokenGameEntity.TYPE_OWNER))
                .findAny()
                .get()
                .getToken();
    }
}

