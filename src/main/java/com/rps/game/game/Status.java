package com.rps.game.game;


public enum Status {
    NONE,
    OPEN,
    ACTIVE,
    WIN,
    LOSE,
    DRAW
}

