package com.rps.game.game;

public enum Sign {
    ROCK,
    PAPER,
    SCISSORS
}
