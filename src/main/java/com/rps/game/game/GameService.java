package com.rps.game.game;

import com.rps.game.exceptions.*;
import com.rps.game.token.TokenEntity;
import com.rps.game.tokenGame.TokenGameEntity;
import com.rps.game.tokenGame.TokenGameRepository;
import com.rps.game.token.TokenRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GameService {

    GameRepository gameRepository;
    TokenRepository tokenRepository;
    TokenGameRepository tokenGameRepository;

    public GameService(GameRepository gameRepository, TokenRepository tokenRepository, TokenGameRepository tokenGameRepository) {
        this.gameRepository = gameRepository;
        this.tokenRepository = tokenRepository;
        this.tokenGameRepository = tokenGameRepository;
    }

    public GameEntity startGame(String tokenId) throws OneGameAtTheTimeAllowedException, JoinerCantHaveTypeOwner {
        TokenEntity token = tokenRepository.getOne(tokenId);
        checkIfMoreThanOneOpenGamesExist(token);

        GameEntity game = createGame();
        gameRepository.save(game);

        TokenGameEntity tokenGame = createNewTokenGameEntity(game, token, TokenGameEntity.TYPE_OWNER);

        saveChanges(token, game, tokenGame);
        return game;
    }


    private void checkIfMoreThanOneOpenGamesExist(TokenEntity token) throws OneGameAtTheTimeAllowedException {
        Stream<TokenGameEntity> openGames = token.getGames().stream()
                .filter(this::tokenTypeEquals)
                .filter(tokenGameEntity -> gameStatusEquals(tokenGameEntity.getGame(), Status.OPEN)
                        && gameStatusEquals(tokenGameEntity.getGame(), Status.ACTIVE));

        if (openGames.count() == 1)
            throw new OneGameAtTheTimeAllowedException();
    }

    private GameEntity createGame() {
        return new GameEntity(UUID.randomUUID().toString(), null, Status.OPEN, null,
                new ArrayList<>());

    }

    public GameEntity joinGame(String gameId, String tokenId) throws TokenAlreadyJoinedToGameException, GameAlreadyStartedException, JoinerCantHaveTypeOwner {
        GameEntity gameToJoin = gameRepository.getOne(gameId);
        TokenEntity joinerToken = tokenRepository.getOne(tokenId);

        checkIfTokenCanJoinGame(gameToJoin, joinerToken);

        TokenGameEntity tokenGame = createNewTokenGameEntity(gameToJoin, joinerToken, TokenGameEntity.TYPE_JOINER);

        gameToJoin.setGame(Status.ACTIVE);
        saveChanges(joinerToken, gameToJoin, tokenGame);
        return gameToJoin;
    }

    private void checkIfTokenCanJoinGame(GameEntity gameToJoin, TokenEntity joinerToken) throws GameAlreadyStartedException, TokenAlreadyJoinedToGameException {
        if (gameToJoin.getTokens().stream()
                .anyMatch(tokenGameEntity -> tokenGameEntity.getToken().equals(joinerToken)))
            throw new TokenAlreadyJoinedToGameException();

        if (gameStatusEquals(gameToJoin, Status.ACTIVE))
            throw new GameAlreadyStartedException();
    }

    private TokenGameEntity createNewTokenGameEntity(GameEntity game, TokenEntity token, String type) {
        TokenGameEntity tokenGame = new TokenGameEntity(
                UUID.randomUUID().toString(),
                token,
                game,
                type
        );
        tokenGameRepository.save(tokenGame);
        return tokenGame;
    }

    public Stream<GameEntity> allOpenGames(String tokenId) {

        return gameRepository.findAll().stream()
                .filter(gameEntity -> gameStatusEquals(gameEntity, Status.OPEN))
                .filter(gameEntity -> !gameEntity.getOwnersTokenId().equals(tokenId));
    }

    public Stream<TokenGameEntity> getAllEndedGamesForToken(String tokenId) {
        return getAllGamesConnectedToToken(tokenId).stream()
                .filter(tokenGameEntity -> tokenGameEntity.getGame().getGame().equals(Status.WIN)
                        || tokenGameEntity.getGame().getGame().equals(Status.LOSE)
                        || tokenGameEntity.getGame().getGame().equals(Status.DRAW))
                ;

    }


    public GameEntity makeMove(Sign sign, String tokenId) throws GameNotFoundException {
        TokenEntity token = tokenRepository.getOne(tokenId);
        TokenGameEntity tokenGame = getTokenGame(token);

        if (!checkIfBothPlayersMadeMove(tokenGame)) {
            setMoves(sign, tokenGame);
        }

        if (checkIfBothPlayersMadeMove(tokenGame))
            tokenGame.getGame()
                    .setGame(calculateGameResult(tokenGame));

        gameRepository.save(tokenGame.getGame());
        return tokenGame.getGame();
    }

    private boolean checkIfBothPlayersMadeMove(TokenGameEntity tokenGame) {
        return tokenGame.getGame().getMove() != null
                && tokenGame.getGame().getOpponentMove() != null;
    }

    private void setMoves(Sign sign, TokenGameEntity tokenGame) {
        if (tokenTypeEquals(tokenGame)) {
            tokenGame.getGame().setMove(sign);
        } else {
            tokenGame.getGame().setOpponentMove(sign);
        }
    }

    public List<TokenGameEntity> getAllGamesConnectedToToken(String tokenId) {
        return tokenRepository.getOne(tokenId).getGames().stream().collect(Collectors.toList());
    }

    private TokenGameEntity getTokenGame(TokenEntity token) throws GameNotFoundException {
        return token.getGames().stream()
                .filter(tokenGameEntity -> tokenGameEntity.getToken().equals(token))
                .filter(tokenGameEntity -> gameStatusEquals(tokenGameEntity.getGame(), Status.ACTIVE))
                .findFirst()
                .orElseThrow(GameNotFoundException::new);
    }

    private boolean tokenTypeEquals(TokenGameEntity tokenGameEntity) {
        return tokenGameEntity.getType().equals(TokenGameEntity.TYPE_OWNER);
    }

    private boolean gameStatusEquals(GameEntity gameEntity, Status status) {
        return gameEntity.getGame().equals(status);
    }

    private void saveChanges(TokenEntity token, GameEntity game, TokenGameEntity tokenGame) throws JoinerCantHaveTypeOwner {
        game.addToken(tokenGame);
        token.addGame(tokenGame);
        gameRepository.save(game);
        tokenRepository.save(token);
    }

    private Status calculateGameResult(TokenGameEntity tokenGame) {
        Sign sign = tokenGame.getGame().getMove();
        Sign opponentSign = tokenGame.getGame().getOpponentMove();

        switch (sign) {
            case ROCK: {
                if (opponentSign.equals(Sign.ROCK))
                    return Status.DRAW;
                if (opponentSign.equals(Sign.PAPER))
                    return Status.LOSE;
                if (opponentSign.equals(Sign.SCISSORS))
                    return Status.WIN;
            }
            case PAPER: {
                if (opponentSign.equals(Sign.PAPER))
                    return Status.DRAW;
                if (opponentSign.equals(Sign.SCISSORS))
                    return Status.LOSE;
                if (opponentSign.equals(Sign.ROCK))
                    return Status.WIN;
            }
            case SCISSORS: {
                if (opponentSign.equals(Sign.SCISSORS))
                    return Status.DRAW;
                if (opponentSign.equals(Sign.ROCK))
                    return Status.LOSE;
                if (opponentSign.equals(Sign.PAPER))
                    return Status.WIN;
            }
        }
        return Status.NONE;
    }


}
