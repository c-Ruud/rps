package com.rps.game.tokenGame;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenGameRepository extends JpaRepository<TokenGameEntity, String> {
}
