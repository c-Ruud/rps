package com.rps.game.token;

import com.rps.game.tokenGame.TokenGameEntity;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity(name = "token")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class TokenEntity {
    @Id String id;
    String name;

    @OneToMany(mappedBy = "token", cascade = CascadeType.ALL)
    List<TokenGameEntity> games;

    public static TokenEntity create() {
        return new TokenEntity(UUID.randomUUID().toString(), "", new ArrayList<>());
    }
    public void setName(String name){
        this.name = name;
    }

    public void addGame(TokenGameEntity tokenGameEntity) {
        games.add(tokenGameEntity);
    }

    @Override
    public String toString() {
        return "TokenEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", games=" + games.stream().map(TokenGameEntity::getType).collect(Collectors.joining()) +
                '}';
    }
}
