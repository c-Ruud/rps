package com.rps.game.token;

import com.rps.game.exceptions.TokenNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

    TokenRepository tokenRepository;

    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public TokenEntity createToken() {
        TokenEntity token = TokenEntity.create();
        tokenRepository.save(token);
        return token;
    }

    public TokenEntity setName(SetName setName, String id) throws TokenNotFoundException {
        TokenEntity tokenToSetName = tokenRepository.findById(id)
                .map(tokenEntity -> {
                    if (setName.getName() != null)
                        tokenEntity.setName(setName.getName());
                    return tokenEntity;
                })
                .orElseThrow(TokenNotFoundException::new);
        tokenRepository.save(tokenToSetName);
        return tokenToSetName;
    }
}


   /* public TokenEntity createToken() {
        TokenEntity token = TokenEntity.create();
        if (checkIfTokenAlreadyExist(token.id))
            return token;
        tokenRepository.save(token);
        return token;
    }

    private boolean checkIfTokenAlreadyExist(String tokenId) {
        TokenEntity token = tokenRepository.getOne(tokenId);
        return  token.getId().isEmpty() ? false: true;
    }*/