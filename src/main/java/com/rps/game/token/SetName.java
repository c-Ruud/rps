package com.rps.game.token;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class SetName {
    String name;

    @JsonCreator
    public SetName(@JsonProperty("name") String name) {
        this.name = name;
    }
}
