package com.rps.game;

import com.rps.game.exceptions.*;
import com.rps.game.game.GameController;
import com.rps.game.game.GameStatus;
import com.rps.game.game.Sign;
import com.rps.game.token.TokenController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("integrationtest")
class RockPaperScissorApplicationTests {

    @Autowired
    GameController gameController;
    @Autowired
    TokenController tokenController;

    @Test
    void test_token() {
        String token = tokenController.createNewToken();

        assertNotNull(token);
    }

    @Test
    @Transactional
    void test_start_game() throws OneGameAtTheTimeAllowedException, JoinerCantHaveTypeOwner {
        // Given
        String tokenId = tokenController.createNewToken();

        // When
        GameStatus game = gameController.createGame(tokenId);

        // Then
        assertNotNull(game);
    }

    @Test
    @Transactional
    void test_Join_game() throws OneGameAtTheTimeAllowedException, GameAlreadyStartedException, TokenAlreadyJoinedToGameException, JoinerCantHaveTypeOwner {
        // Given
        String tokenId = tokenController.createNewToken();
        String joinerTokenId = tokenController.createNewToken();
        GameStatus game = gameController.createGame(tokenId);

        // When
        gameController.joinGame(game.getId(), joinerTokenId);

        // Then
        assertNotNull(game);
    }

    @Test
    @Transactional
    void test_makeMove_owner() throws OneGameAtTheTimeAllowedException, GameAlreadyStartedException, TokenAlreadyJoinedToGameException, GameNotFoundException, JoinerCantHaveTypeOwner {
        // Given
        String tokenId = tokenController.createNewToken();
        String joinerTokenId = tokenController.createNewToken();
        GameStatus game = gameController.createGame(tokenId);
        gameController.joinGame(game.getId(), joinerTokenId);

        // When
        gameController.makeMove(Sign.PAPER, tokenId);
        // Then
        assertNotNull(game);
    }

    @Test
    @Transactional
    void test_makeMove_joiner() throws OneGameAtTheTimeAllowedException, GameAlreadyStartedException, TokenAlreadyJoinedToGameException, GameNotFoundException, JoinerCantHaveTypeOwner {
        // Given
        String tokenId = tokenController.createNewToken();
        String joinerTokenId = tokenController.createNewToken();
        GameStatus game = gameController.createGame(tokenId);
        gameController.joinGame(game.getId(), joinerTokenId);

        // When
        gameController.makeMove(Sign.SCISSORS, joinerTokenId);
        // Then
        assertNotNull(game);
    }
}
