package com.rps.game;

import com.rps.game.exceptions.*;
import com.rps.game.game.*;
import com.rps.game.token.SetName;
import com.rps.game.token.TokenController;
import com.rps.game.token.TokenRepository;
import com.rps.game.tokenGame.TokenGameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import javax.transaction.Transactional;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("integrationtest")
class GameControllerTest {

    @Autowired
    TokenController tokenController;
    @Autowired
    GameController gameController;
    @Autowired
    GameService gameService;
    @Autowired
    TokenRepository tokenRepository;
    @Autowired
    TokenGameRepository tokenGameRepository;

    @BeforeEach
    void setUp() {
         tokenRepository.deleteAll();
    }

    @Test
    @Transactional
    void create_game_without_name_success() throws JoinerCantHaveTypeOwner, OneGameAtTheTimeAllowedException {
        // Given
        String tokenId = tokenController.createNewToken();

        // When
        GameStatus gameStatus = gameController.createGame(tokenId);

        // Then
        assertNotNull(gameStatus.getId());
        assertEquals("", gameStatus.getName());
        assertEquals("", gameStatus.getMove());
        assertEquals("OPEN", gameStatus.getGame());
        assertEquals("", gameStatus.getOpponentName());
        assertEquals("", gameStatus.getOpponentMove());
    }
    @Test
    @Transactional
    void create_game_with_name_success() throws JoinerCantHaveTypeOwner, OneGameAtTheTimeAllowedException, TokenNotFoundException {
        // Given
        String tokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("BOB"), tokenId);

        // When
        GameStatus gameStatus = gameController.createGame(tokenId);

        // Then
        assertNotNull(gameStatus.getId());
        assertEquals("BOB", gameStatus.getName());
        assertEquals("", gameStatus.getMove());
        assertEquals("OPEN", gameStatus.getGame());
        assertEquals("", gameStatus.getOpponentName());
        assertEquals("", gameStatus.getOpponentMove());
    }

  /*  @Test
    @Transactional
    void create_second_game_fail_because_one_game_at_time_allowed() throws JoinerCantHaveTypeOwner, OneGameAtTheTimeAllowedException, TokenNotFoundException {
        // Given
        String tokenId = tokenController.createNewToken();


        // When

            gameController.createGame(tokenId);
            gameController.createGame(tokenId);


        // Then
        assertThrows(OneGameAtTheTimeAllowedException, gameController.createGame(tokenId));

    }*/

    @Test
    @Transactional
    void join_game_success() throws JoinerCantHaveTypeOwner, OneGameAtTheTimeAllowedException, GameAlreadyStartedException, TokenAlreadyJoinedToGameException, TokenNotFoundException {
        // Given
        String tokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Sten"), tokenId);
        String joinerTokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Birgitta"), joinerTokenId);
        GameStatus game = gameController.createGame(tokenId);

        // When
        GameStatus gameStatus = gameController.joinGame(game.getId(), joinerTokenId);

        // Then
        assertNotNull(gameStatus.getId());
        assertEquals("Birgitta", gameStatus.getName());
        assertEquals("", gameStatus.getMove());
        assertEquals("ACTIVE", gameStatus.getGame());
        assertEquals("Sten", gameStatus.getOpponentName());
        assertEquals("", gameStatus.getOpponentMove());
    }

    @Test
    @Transactional
    void get_game_status_by_token_id_success() throws TokenNotFoundException, JoinerCantHaveTypeOwner, OneGameAtTheTimeAllowedException, GameAlreadyStartedException, TokenAlreadyJoinedToGameException {
        // Given
        String tokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Sten"), tokenId);
        String joinerTokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Birgitta"), joinerTokenId);
        GameStatus game = gameController.createGame(tokenId);
        gameController.joinGame(game.getId(), joinerTokenId);

        //When
        GameStatus gameStatus = gameController.statusGame(tokenId);

        //Then
        assertNotNull(gameStatus.getId());
        assertEquals("Sten", gameStatus.getName());
        assertEquals("", gameStatus.getMove());
        assertEquals("ACTIVE", gameStatus.getGame());
        assertEquals("Birgitta", gameStatus.getOpponentName());
        assertEquals("", gameStatus.getOpponentMove());
    }

    @Test
    @Transactional
    void get_list_of_all_open_games_success() throws JoinerCantHaveTypeOwner, OneGameAtTheTimeAllowedException, TokenNotFoundException {
        // Given
        String tokenId = tokenController.createNewToken();
        String viewerToken = tokenController.createNewToken();
        tokenController.setName(new SetName("Birgitta"), tokenId);
        gameController.createGame(tokenId);

        // When
        List<GameStatus> token = gameController.listOfAllJoinableGames(viewerToken);

        // Then
        assertEquals(1, token.size());
        assertNotNull(token.get(0).getId());
        assertEquals("Birgitta", token.get(0).getName());
        assertEquals("", token.get(0).getMove());
        assertEquals("OPEN", token.get(0).getGame());
        assertEquals("", token.get(0).getOpponentName());
        assertEquals("", token.get(0).getOpponentMove());
    }

    @Test
    @Transactional
    void get_game_status_by_id_success() throws TokenNotFoundException, TokenAlreadyJoinedToGameException, JoinerCantHaveTypeOwner, GameAlreadyStartedException, OneGameAtTheTimeAllowedException {
        // Given
        String tokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Sten"), tokenId);
        String joinerTokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Birgitta"), joinerTokenId);
        GameStatus game = gameController.createGame(tokenId);
        gameController.joinGame(game.getId(), joinerTokenId);

        //When
        GameStatus gameStatus = gameController.getGameStatus(tokenId, game.getId());

        //Then
        assertNotNull(gameStatus.getId());
        assertEquals("Sten", gameStatus.getName());
        assertEquals("", gameStatus.getMove());
        assertEquals("ACTIVE", gameStatus.getGame());
        assertEquals("Birgitta", gameStatus.getOpponentName());
        assertEquals("", gameStatus.getOpponentMove());
    }

    @Test
    @Transactional
    void owner_makes_move_success() throws OneGameAtTheTimeAllowedException, GameAlreadyStartedException, TokenAlreadyJoinedToGameException, GameNotFoundException, JoinerCantHaveTypeOwner, TokenNotFoundException {
        // Given
        String tokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Sten"), tokenId);
        String joinerTokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Birgitta"), joinerTokenId);
        GameStatus game = gameController.createGame(tokenId);
        gameController.joinGame(game.getId(), joinerTokenId);

        // When
        GameStatus gameStatus = gameController.makeMove(Sign.PAPER, tokenId);

        //Then
        assertNotNull(gameStatus.getId());
        assertEquals("Sten", gameStatus.getName());
        assertEquals("PAPER", gameStatus.getMove());
        assertEquals("ACTIVE", gameStatus.getGame());
        assertEquals("Birgitta", gameStatus.getOpponentName());
        assertEquals("", gameStatus.getOpponentMove());
    }

    @Test
    @Transactional
    void joiner_makes_move_and_calculate_game_result_success() throws OneGameAtTheTimeAllowedException, GameAlreadyStartedException, TokenAlreadyJoinedToGameException, GameNotFoundException, JoinerCantHaveTypeOwner, TokenNotFoundException {
        // Given
        String tokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Sten"), tokenId);
        String joinerTokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Birgitta"), joinerTokenId);
        GameStatus game = gameController.createGame(tokenId);
        gameController.joinGame(game.getId(), joinerTokenId);

        // When
        gameController.makeMove(Sign.PAPER, tokenId);
        GameStatus gameStatus = gameController.makeMove(Sign.SCISSORS, joinerTokenId);

        //Then
        assertNotNull(gameStatus.getId());
        assertEquals("Birgitta", gameStatus.getName());
        assertEquals("SCISSORS", gameStatus.getMove());
        assertEquals("WIN", gameStatus.getGame());
        assertEquals("Sten", gameStatus.getOpponentName());
        assertEquals("PAPER", gameStatus.getOpponentMove());
    }

    @Test
    @Transactional
    void playedGamesHistory() throws TokenNotFoundException, OneGameAtTheTimeAllowedException, JoinerCantHaveTypeOwner, GameNotFoundException, TokenAlreadyJoinedToGameException, GameAlreadyStartedException {
        // Given
        String tokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Sten"), tokenId);
        String joinerTokenId = tokenController.createNewToken();
        tokenController.setName(new SetName("Birgitta"), joinerTokenId);
        GameStatus game = gameController.createGame(tokenId);
        gameController.joinGame(game.getId(), joinerTokenId);
        gameController.makeMove(Sign.PAPER, tokenId);
        gameController.makeMove(Sign.SCISSORS, joinerTokenId);

        // When
        List<GameStatus> gameStatus = gameController.playedGamesHistory(tokenId);

        // Then
        assertNotNull(gameStatus.get(0).getId());
        assertEquals("Sten", gameStatus.get(0).getName());
        assertEquals("PAPER", gameStatus.get(0).getMove());
        assertEquals("LOSE", gameStatus.get(0).getGame());
        assertEquals("Birgitta", gameStatus.get(0).getOpponentName());
        assertEquals("SCISSORS", gameStatus.get(0).getOpponentMove());
    }
}
