package com.rps.game;

import com.rps.game.exceptions.TokenNotFoundException;
import com.rps.game.token.SetName;
import com.rps.game.token.TokenController;
import com.rps.game.token.TokenRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@ActiveProfiles("integrationtest")
class TokenControllerTest {

    @Autowired
    TokenController tokenController;

    @Autowired
    TokenRepository tokenRepository;

    @Test
    void create_new_token_success() {
        // Given
        String token = tokenController.createNewToken();

        // Then
        assertNotNull(token);
    }

    @Test
    @Transactional
    void set_token_name_success() throws TokenNotFoundException {
        // Given
        String tokenId = tokenController.createNewToken();

        //When
        tokenController.setName(new SetName("BOB"), tokenId);

        //Then
        assertEquals("BOB", tokenRepository.getOne(tokenId).getName());

    }

}
