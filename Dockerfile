FROM adoptopenjdk/openjdk11:latest
COPY target/game-0.0.1-SNAPSHOT.jar app.jar
COPY src/main/resources/application.properties /config/
VOLUME /config
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar","--spring.config.location=/config/application.properties"]


